package com.youtube.childapp;

import com.youtube.childapp.Database.DatabaseHelper;

public class Application extends android.app.Application {
    public static DatabaseHelper databaseHelper;
    @Override
    public void onCreate() {
        super.onCreate();
        databaseHelper = new DatabaseHelper(this);
    }
}

package com.youtube.childapp.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "video_kienmt.sqlite";
    private static String TABLE_WORD = "video_kienmt";
    private static String TABLE_NAME = "Video";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createQuery = "CREATE TABLE IF NOT EXISTS " +
                "Video(" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "fullname TEXT ," +
                "url TEXT )";
        db.execSQL(createQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WORD);
        onCreate(db);
    }

    public void insertADraftImage(String _name, String _url) {

        SQLiteDatabase db = this.getWritableDatabase();
        String query = "Insert into " + TABLE_NAME + "(fullname,url) values " +
                "( "
                + "'" + _name + "', "
                + "'" + _url + "');";
        db.execSQL(query);
        close();
    }
}
